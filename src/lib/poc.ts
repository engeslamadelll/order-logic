enum ORDER_TYPE {
  BUY = "BUY",
  SELL = "SELL",
}

enum ORDER_STATUS {
  QUEUED_SUBMIT = "QUEUED_SUBMIT",
  QUEUED_CANCEL = "QUEUED_CANCEL",
  PENDING = "PENDING",
  CLOSED = "CLOSED",
  COMPLETED = "COMPLETED",
}

const THNDR_FEES = 0;

const LIMIT_ORDER_MARGIN = 5;

function calculateStockPrice({
  is_limit,
  limitPrice,
  is_ipo,
  ipo_price,
  marketPrice,
}: {
  is_limit: boolean;
  limitPrice: number;
  is_ipo: boolean;
  ipo_price: number;
  marketPrice: number;
}): number {
  return is_limit ? limitPrice : is_ipo ? ipo_price : marketPrice;
}

function caculatePurchasePower({
  is_editing,
  amount,
  price,
  purchasePower,
}: {
  is_editing: boolean;
  amount?: number;
  price?: number;
  feesWhileEditing?: number;
  purchasePower?: number;
}): number {
  if (is_editing) {
    const feesWhileEditing = Math.max(5, 0.002 * (price ?? 0) * (amount ?? 0));
    return purchasePower + (price ?? 0) * (amount ?? 0) + feesWhileEditing;
  } else {
    return purchasePower;
  }
}

function calculateMargin({
  type,
  amount,
  price,
  units,
}: {
  type: ORDER_TYPE;
  amount?: number;
  price?: number;
  units?: number;
}): number {
  if (type === ORDER_TYPE.BUY) {
    if (amount) {
      return Math.max(0.03 * amount, 10);
    } else if (price && units) {
      return Math.max(0.03 * (price * units), 10);
    } else {
      return 0;
    }
  } else {
    return 0;
  }
}

function calculateFees({
  amount,
  price,
  units,
}: {
  amount?: number;
  price?: number;
  units?: number;
}): number {
  if (amount) {
    return Math.max(0.00005 * amount, 1) + 0.00075 * amount + THNDR_FEES;
  } else if (price && units) {
    return (
      Math.max(0.00005 * (price * units), 1) +
      0.00075 * (price * units) +
      THNDR_FEES
    );
  } else {
    return 0;
  }
}

function getMaxAmount({
  purchasePower,
  stockPrice,
  availableUnits,
  orderType,
  isIPO,
}: {
  purchasePower: number;
  stockPrice: number | undefined;
  availableUnits: number;
  orderType: any;
  isIPO?: boolean;
}): number {
  if (orderType === ORDER_TYPE.BUY) {
    if (isIPO) {
      return +purchasePower * 4;
    } else {
      return +purchasePower;
    }
  } else {
    if (stockPrice !== undefined) {
      return +(availableUnits * stockPrice);
    } else {
      return 0;
    }
  }
}

function getMaxShares({
  amount,
  stockPrice,
  orderType,
  availableUnits,
}: {
  amount: number;
  stockPrice: number | undefined;
  orderType: any;
  availableUnits: number;
}): number {
  const fees =
    calculateMargin({ type: orderType, amount: amount }) +
    calculateFees({ amount: amount });
  if (stockPrice !== undefined) {
    if (orderType === ORDER_TYPE.BUY) {
      if (fees > amount) {
        return 0;
      } else {
        return (amount - fees) / stockPrice;
      }
    } else if (orderType === ORDER_TYPE.SELL) {
      return availableUnits;
    } else {
      return 0;
    }
  } else {
    return 0;
  }
}

function getSharesFromAmount({
  amount,
  stockPrice,
  orderType,
}: {
  amount: number;
  stockPrice: number | undefined;
  orderType: ORDER_TYPE;
}): number {
  const fees = calculateFees({ amount, price: stockPrice });
  if (stockPrice !== undefined) {
    if (orderType === ORDER_TYPE.BUY) {
      if (fees < amount) {
        return (+amount - fees) / stockPrice;
      } else {
        return 0;
      }
    } else {
      return (+amount + fees) / stockPrice;
    }
  } else {
    return 0;
  }
}

function getAmountFromShares({
  units,
  stockPrice,
  orderType,
  fractionable = false,
}: {
  units: number;
  stockPrice: number | undefined;
  orderType: ORDER_TYPE;
  fractionable?: boolean;
}): number {
  const fees = calculateFees({ price: stockPrice, units });

  if (stockPrice !== undefined) {
    if (orderType === ORDER_TYPE.BUY) {
      if (!+units) {
        return 0;
      } else {
        let amount = units * stockPrice + fees;
        return fractionable ? amount : +(units * stockPrice + fees).toFixed(2);
      }
    } else {
      let amount = units * stockPrice - fees;
      if (amount < 0) {
        return 0;
      } else {
        return fractionable ? amount : +amount.toFixed(2);
      }
    }
  } else {
    return 0;
  }
}

function getMaxSharesLimitPrice({
  amount,
  stockPrice,
  orderType,
  availableUnits,
}: {
  amount: number;
  stockPrice: number | undefined;
  orderType: any;
  availableUnits: number;
}): number {
  const fees = Math.max(LIMIT_ORDER_MARGIN, 0.002 * amount);
  if (stockPrice !== undefined) {
    if (orderType === ORDER_TYPE.BUY) {
      if (fees > amount) {
        return 0;
      } else {
        return Math.floor((amount - fees) / stockPrice);
      }
    } else if (orderType === ORDER_TYPE.SELL) {
      return Math.floor(availableUnits);
    } else {
      return 0;
    }
  } else {
    return 0;
  }
}
function calcEstimatedTotal({ order }: { order: any }) {
  if (order.order_status === ORDER_STATUS.COMPLETED) {
    return order.price * order.amount_filled;
  } else {
    return order.price * order.amount;
  }
}

export {
  ORDER_TYPE,
  ORDER_STATUS,
  THNDR_FEES,
  LIMIT_ORDER_MARGIN,
  calculateStockPrice,
  caculatePurchasePower,
  calculateMargin,
  calculateFees,
  getMaxAmount,
  getMaxShares,
  getSharesFromAmount,
  getAmountFromShares,
  getMaxSharesLimitPrice,
  calcEstimatedTotal,
};
