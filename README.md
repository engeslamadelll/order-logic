# order-logic

# Installation

```bash
yarn add order-logic
```

# Usage

```
import {
  calculateFees,
  getMaxAmount,
  getMaxShares,
  getMaxSharesLimitPrice,
  caculatePurchasePower,
  calculateStockPrice,
} from 'order-logic';
```
